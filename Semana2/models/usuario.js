const mon = require('mongoose');
const Reserva = require('./reserva');

const { Schema, model } = mon;

const validatioEmail = function (email) {
    const re = /^\w([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+s/;
    return re.test(email);
};

const usuarioScehma = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es requerido']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El correo es requerido'],
        lowercase: true,
        validate: [validatioEmail, 'por favor', 'ingrese un email valido'],
        match: [/^\w([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+s/]
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpiress: Date,
    verificado: {
        type: Boolean,
        default: false
    }
});

usuarioScehma.pre('save', function (next) {
    if (this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
})

usuarioScehma.methods.reservar = function (biciId, desde, hasta, cb) {
    var reserva = new Reserva({ usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta })
    console.log(reserva);
    reserva.save(cb);
}

module.exports = model("usuariosbici", usuarioScehma); 