const usu = require('../../models/usuario');

exports.usuarios_list = function (req, res) {
    usu.find({}, function (err, users) {
        res.json({ usuarios: users })
    })
}

exports.usuarios_crear = function (req, res) {
    var usuario = new usu({ nombre: req.body.nombre });
    usuario.save(function (err) {
        res.json(usuario);
    });
}

exports.usurios_reserva = function (req, res) {
    const { id, bici_id, desde, hasta } = req.body;
    usu.findById(id, (err, usuario) => {
        console.log(usuario);
        usuario.reservar(bici_id, desde, hasta, function (err) {
            res.json({ usuario });
        })
    })
}