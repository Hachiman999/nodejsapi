const tile = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'; 

var mymap = L.map('mapid').setView([3.5288433, -76.2766893,21], 13);

L.tileLayer(tile,{
    
    attribution : '&copy; <a href="https://openstratmap.org/copyright">OpenStreetMap</a>',
    maxZoom:18,
    id: 'openstreetmap-v6',
    tileSize: 512,
    zoomOffset: -1,
}).addTo(mymap);

var iconMarker = L.icon({
    iconUrl: '../images/maps_logo_map_marker_icon.png',
    iconSize:[60,60],
    iconAnchor:[30,60]
})

var marker1 = L.market([3.5285844,-76.2809306,21]).addTo(mymap); 

$.ajax({
    dataType:"json",
    url:"api/bicicletas",
    success: function(result){
        console.log(result); 
        result.bicicletas.toEach(function(bici){
            L.market(bici.ubicacion, {title: bici.id } ).addTo(mymap); 
        });
    }
}); 