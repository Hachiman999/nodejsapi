const user = require('../models/usuario');
const Token = require('../models/token');

module.exports = {
    confirmationGet: function (req, res, next) {
        const { token } = req.params;
        Token.findOne({ token }, function (err, token) {
            if (!token) { return res.status(400).send({ type: 'not-verified', msg: "no se escontro este usuario" }) }
            user.findById(token._userId, function (err, usuario) {
                if (!usuario) { return res.status(400).send({ msg: "usuario no encontrado" }) }
                if (usuario.verificado) return res.redirect('/usuarios');
                usuario.verificado = true;
                usuario.save(function (err) {
                    if (err) { return res.status(500).send({ msg: err.message }) }
                    res.redirect('/')
                });
            });
        });
    }
}