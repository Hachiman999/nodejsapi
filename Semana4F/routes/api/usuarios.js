var express = require('express');
var router = express.Router();
var controller = require('../../controllers/api/userControllers');

router.get('/', controller.usuarios_list);;
router.post('/create', controller.usuarios_crear);
router.post('/reservar', controller.usurios_reserva);
module.exports = router; 