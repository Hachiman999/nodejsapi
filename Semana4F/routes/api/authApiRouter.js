const express = require('express');
const router = express.Router()
const authApiController = require('../../controllers/api/authAPIController');

router.post('/authenticate', authApiController.authenticate)
router.post('/forgotPassword', authApiController.forgotPassword)

module.exports = router