const ex = require('express');

const router = ex.Router();

const tokencontrooler = require('../controllers/token');

router.get('/confirmation/:token', tokencontrooler.confirmationGet);

module.exports = router; 