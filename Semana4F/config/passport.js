const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const Usuario = require('../models/usuario')
const GoogleStrategy = require('passport-google-oauth20');
passport.use(new LocalStrategy(
    function (email, password, done) {
        Usuario.findOne({ email: email }, (err, usuario) => {
            if (err) return done(err)
            if (!usuario) return done(null, false, { message: `Email incorrecto` })
            if (!usuario.validPassword(password)) return done(null, false, { message: 'Password incorrecto' })

            return done(null, usuario)
        })
    }
))


passport.use(new GoogleStrategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: process.env.HOST + "/auth/google/callback"
},
    function (accesToken, refreshToken, profile, cb) {
        console.log(profile);
        Usuario.findOneCreateByGoogle(profile, function (err, user) {
            return cb(err, user);
        })
    }

));


passport.serializeUser(function (usuario, cb) {
    console.log(usuario.id)
    cb(null, usuario.id)
})

passport.deserializeUser(function (id, cb) {
    Usuario.findById(id, function (err, usuario) {
        cb(err, usuario)
    })
})

module.exports = passport