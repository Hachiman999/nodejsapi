const { Schema, model } = require('mongoose');

const TokenSchema = new Schema({
    _userId: { type: Schema.Types.ObjectId, required: true, ref: 'usuariosbici' },
    token: { type: String, required: true },
    createAt: { type: Date, required: true, default: Date.now, expires: 43200 }
})


module.exports = model('Token', TokenSchema); 