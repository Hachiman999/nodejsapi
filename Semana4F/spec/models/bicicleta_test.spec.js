const mon = require('mongoose');
const Bici = require('../../models/bicicleta');



describe('TESTING biciclestas', function () {
    beforeEach(function (done) {
        const KEY_DB = "mongodb+srv://tvbotbdproyecto:5UV2qVL3XWrlL76k@cluster0.7h1um.mongodb.net/tvvideo?retryWrites=true&w=majority";
        mon.connect(KEY_DB, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });

        const DB = mon.connection;
        DB.on('error', console.error.bind(console, 'conection error'));
        DB.once('open', function () {
            console.log('we are connected to test databse');
            done();
        });
    });

    afterEach(function (done) {
        Bici.deleteMany({}, function (err, sucess) {
            if (err) console.log(err);
            mon.disconnect(err); 
            done();
        });
    });


    describe('Bici.createInstace', () => {
        it('Creat one instance to Bici', (done) => {
            var bici = Bici.createInstace(1, "verde", "urbano", [-34.5, -54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbano");
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.1);

            done();
        })
    });



    describe('Bici.allBicis', () => {
        it('beging null', (done) => {
            Bici.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });
    describe('Bici.findBycode', () => {
        it('the code 1 is searched', (done) => {
            Bici.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici = new Bici({code:1,color:"verde", modelo: "urbana" })
                Bici.add(aBici, function (err, newbici) {
                    if (err) console.log(err);
                    Bici.findBycode(1,function(err, target){
                        expect(target.code).toBe(aBici.code);
                        expect(target.color).toBe(aBici.color);
                        expect(target.modelo).toBe(aBici.modelo);
                        done();
                    });
                })


               
            });
        })
    });
    describe('Bici.add', () => {
        it('add one bici', (done) => {
            var onebici = new Bici({ code: 1, color: "verde", modelo: "urbana" });
            Bici.add(onebici, function (err, newbici) {
                if (err) console.log(err);
                Bici.allBicis(function (err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(onebici.code);
                    done();
                });
            })
        })
    })
  


  
});//fin testing bicicleta
  /*
        describe('Bici.allBicis', () => {
            it('beging null', (done) => {
                Bici.allBicis(function (err, bicis) {
                    expect(bicis.length).toBe(0);
                    done();
                });
            });
        });
    
        describe('Bici.add', () => {
            it('add one bici', (done) => {
                var onebici = new Bici({ code: 1, color: "verde", modelo: "urbana" });
                Bici.add(onebici, function (err, newbici) {
                    if (err) console.log(err);
                    Bici.allBicis(function (err, bicis) {
                        expect(bicis.length).toEqual(1);
                        expect(bicis[0].code).toEqual(onebici.code);
                        done();
                    });
                })
            })
        })*/

/*
beforeEach(()=>{
    Bici.allBicis=[];
});

describe('Bici.allbics',()=>{
    it('comiensa bacia',()=>{
        expect(Bici.allBicis.length).toBe(0);
    });
});

describe('Bici.add',()=>{
    it('comiensa bacia',()=>{
        expect(Bici.allBicis.length).toBe(0);

        Bici.add( new Bici(1,'rojo','urbana',[3.5285844,-76.2809306,21] ));
        expect(Bici.allBicis.length).toBe(1);
    });
});

describe('indByid',()=>{
    it('comiensa bacia',()=>{
        Bici.add( new Bici(2,'rojo','urbana',[3.5285844,-76.2809306,21] ));
        Bici.add( new Bici(3,'rojo','urbana',[3.5285844,-76.2809306,21] ));
        expect(Bici.findByid(2).id).toBe(2);
        expect(Bici.findByid(3).id).toBe(3);

    });
});

*/