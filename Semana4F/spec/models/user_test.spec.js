const mon = require('mongoose');
const Bici = require('../../models/bicicleta')
const User = require('../../models/usuario');
const Reserva = require('../../models/reserva');

describe('testin Users', function () {
    beforeEach(function (done) {
        const KEY = 'mongodb+srv://tvbotbdproyecto:5UV2qVL3XWrlL76k@cluster0.7h1um.mongodb.net/tvvideo?retryWrites=true&w=majority';
        mon.connect(KEY, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
        const DB = mon.connection;
        DB.on('error', console.error.bind(console, 'connection error'));
        DB.once('open', function () { console.log('We are connected to test database!'); done(); });
    });


    afterEach(function (done) {
        Reserva.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            User.deleteMany({}, function (err, success) {
                if (err) console.log(err);
                Bici.deleteMany({}, function (err, success) {
                    if (err) console.log(err);
                    mon.disconnect()
                    done();
                });
            });
        });
        //mon.disconnect()
        //done();
    });

    describe('when a user booking a bike', () => {
        it('since the reservation existed', (done) => {
            const usuario = new User({ nombre: "minombre" });
            usuario.save();
            const bicicleta = new Bici({ code: 1, color: "verde", modelo: "urbana" });
            bicicleta.save();

            var hoy = new Date();
            var mañana = new Date();

            mañana.setDate(hoy.getDate() + 1);
            usuario.reservar(bicicleta.id, hoy, mañana, function (err, success) {
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function (err, reservas) {
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    })


});

