const Bici = require('../../models/bicicleta');
const req = require('request');
const server = require('../../bin/www');
const mon = require('moongose');

const URL = "http://localhost:5000/api"
describe('Bicicleta Api', () => {

    beforeEach(function (done) {
        const KEY_DB = "mongodb+srv://tvbotbdproyecto:5UV2qVL3XWrlL76k@cluster0.7h1um.mongodb.net/tvvideo?retryWrites=true&w=majority";
        mon.connect(KEY_DB, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });

        const DB = mon.connection;
        DB.on('error', console.error.bind(console, 'conection error'));
        DB.once('open', function () {
            console.log('we are connected to test databse');
            done();
        });
    });

    describe('get', () => {
        it('Status 200', () => {
            expect(Bici.allBicis.length).toBe(0);
            Bici.add(new Bici(1, 'rojo', 'urbana', [3.5285844, -76.2809306, 21]));
            req.get(`${URL}/bicicletas`, function (error, response, body) {
                response.statusCode.toBe(200);
            });
        });
    });

    afterEach(function (done) {
        Bici.deleteMany({}, function (err, sucess) {
            if (err) console.log(err);
            mon.disconnect(err);
            done();
        });
    });

    describe('post', (done) => {
        const headers = { 'content-type': 'application/json' };
        const aBici = '{ "id" : 10, "color": "rojo", "modelo": "urbana", "lat": -34, "log": -54 }';
        req.post({
            headers: headers,
            url: `${URL}/create`,
            body: aBici
        }, function (error, response, body) {
            expect(response.statusCode).toBe(200);
            expect(Bici.findByid(10).color).toBe("rojo");
            done();
        });

    });
}); 