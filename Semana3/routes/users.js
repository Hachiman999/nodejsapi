var express = require('express');
var router = express.Router();
const usuarios = require('../controllers/usuarios');
/* GET users listing. */
router.get('/', usuarios.list);
router.get('/create', usuarios.create_get);
router.post('/create', usuarios.create);
router.get('/:id/update', usuarios.update_get);
router.post('/:id/update', usuarios.update);
router.post('/:id/delete', usuarios.delete);
module.exports = router;
