/*const tile = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';

var mymap = L.map('mapid').setView([3.5288433, -76.2766893, 21], 13);

L.tileLayer(tile, {

    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    maxZoom: 18,
    id: 'openstreetmap-v6',
    tileSize: 512,
    zoomOffset: -1,
}).addTo(mymap);

var iconMarker = L.icon({
    iconUrl: '../images/maps_logo_map_marker_icon.png',
    iconSize: [60, 60],
    iconAnchor: [30, 60]
})

var marker1 = L.market([3.5285844, -76.2809306, 21]).addTo(mymap);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function (result) {
        console.log(result);
        result.bicicletas.toEach(function (bici) {
            L.market(bici.ubicacion, { title: bici.id }).addTo(mymap);
        });
    }
});*/
var map = L.map('main_map').setView([-27.38280502782093, -55.9495353433769], 13)
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);


// L.marker([-27.37280502782093, -55.8959353433769]).addTo(map)
// L.marker([-27.38280502782093, -55.9275353433769]).addTo(map)
// L.marker([-27.38280502782093, -55.9475353433769]).addTo(map)


$.ajax({
    dataType: 'json',
    url: 'http://localhost:5000/api/bicicletas',
    success: function (result) {
        console.log(result)
        result.bicicletas.forEach(function (bici) {
            L.marker(bici.ubicacion, { title: bici.code }).addTo(map)
                .bindPopup('Posadas, Misiones, Argentina')
                .openPopup();
            // console.log(bici.ubicacion)
        })
    }
})



