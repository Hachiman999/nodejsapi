const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const Usuario = require('../models/usuario')

passport.use(new LocalStrategy(
    function (email, password, done) {
        Usuario.findOne({ email: email }, (err, usuario) => {
            if (err) return done(err)
            if (!usuario) return done(null, false, { message: `Email incorrecto` })
            if (!usuario.validPassword(password)) return done(null, false, { message: 'Password incorrecto' })

            return done(null, usuario)
        })
    }
))

passport.serializeUser(function (usuario, cb) {
    console.log(usuario.id)
    cb(null, usuario.id)
})

passport.deserializeUser(function (id, cb) {
    Usuario.findById(id, function (err, usuario) {
        cb(err, usuario)
    })
})

module.exports = passport