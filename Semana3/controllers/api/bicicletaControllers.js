const Bicicleta = require("../../models/bicicleta");

exports.bicicleta_list = function (req, res) {
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
}

exports.bicicleta_create = async function (req, res) {
    const { color, modelo, lat, log } = req.body;
    const ubicacion = [lat, log];
    var bici = Bicicleta.createInstace(1, color, modelo, ubicacion);
    bici.save(function (err) {
        res.json({ bicicleta: bici });
    });

}

/**
 * const { color, modelo, lat, log } = req.body;
    var bici = new Bicicleta(1, color, modelo);
    bici.ubicacion = [lat, log];

    Bicicleta.add(bici);
    res.status(200).json({
        bicicleta: bici
    });
*/