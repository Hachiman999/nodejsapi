const modelUsers = require('../models/usuario');

module.exports = {
    list: function (req, res, next) {
        modelUsers.find({}, (err, usuarios) => {
            res.render('usuarios/index', { usuarios });
        })
    },
    update_get: function (req, res, next) {
        modelUsers.findById(req.params.id, function (err, usuario) {
            res.render('usuarios/update', { erros: {}, usuario })
        });
    },
    update: function (req, res, next) {
        var up = { nombre: req.body.nombre };
        modelUsers.findByIdAndUpdate(req.params.id, up, function (err, usuario) {
            if (err) {
                console.log(err);
                res.render('usuarios/create', { errors: err.errors, usuario: new modelUsers({ nombre: req.body.nombre, email: req.body.email }) });
            } else {
                res.redirect('/usuarios');
                return;
            }
        });
    },
    create_get: function (req, res, next) {
        console.log("entre")
        res.render("usuarios/create", {
            errors: {},
            usuario: new modelUsers()
        });
    },
    create: function (req, res, next) {

        if (req.body.password != req.body.confirm_password) {
            res.render('usuarios/create', { errors: { confirm_pass: { message: 'Las passwords no coinciden' } }, usuario: new modelUsers({ nombre: req.body.nombre, email: req.body.email }) })
            return
        }

        modelUsers.create({ nombre: req.body.nombre, email: req.body.email, password: req.body.password }, (err, nuevoUsuario) => {
            if (err) {
                console.log(err.errors);
                res.render('usuarios/create', { errors: err.errors, usuario: new modelUsers({ nombre: req.body.nombre, email: req.body.email }) })
            } else {
                nuevoUsuario.enviar_email_bienvenida()
                res.redirect('/usuarios')
            }
        })
    },
    delete: function (req, res, next) {
        const { id } = req.body;
        modelUsers.findByIdAndDelete(id, function (err) {
            if (err) {
                next(err);
            } else {
                res.redirect('/usuarios');
            }
        });
    }
}