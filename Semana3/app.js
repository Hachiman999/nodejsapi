var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const passport = require('./config/passport');
const session = require('express-session');
const jwt = require('jsonwebtoken');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var tokenrouter = require('./routes/token');
var bicicletaRouter = require('./routes/bicicletas');
var bicicletasAPIRouter = require('./routes/api/bicicletas');
var userAPIRouter = require('./routes/api/usuarios');
const Usuario = require('./models/usuario');
const Token = require('./models/token');
const authApiRouter = require('./routes/api/authApiRouter')
const mon = require('mongoose');

const store = new session.MemoryStore;
var app = express();
app.set('secretKey', '123456789')
app.use(
  session({
    cookie: { maxAge: 240 * 60 * 60 * 1000 },
    store: store,
    resave: true,
    saveUninitialized: true,
    secret: 'mi secreto'
  })
)

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize())
app.use(passport.session())
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
//app.use('/users', usersRouter);
app.use('/usuarios', usersRouter);
app.use('/token', tokenrouter);
app.use('/bicicletas', bicicletaRouter);

app.use('/api/bicicletas', validarUsuario, bicicletasAPIRouter);
app.use('/api/usuarios', userAPIRouter);
app.use('/api/auth', authApiRouter);



app.get('/login', function (req, res) {
  res.render('session/login')
})

app.post('/login', function (req, res, next) {
  passport.authenticate('local', function (err, usuario, info) {
    if (err) return next(err)
    if (!usuario) return res.render('session/login', { info })

    req.logIn(usuario, function (err) {
      if (err) return next(err)
      return res.redirect('/')
    })
  })(req, res, next);
})

app.get('/logout', function (req, res) {
  req.logOut()
  res.redirect('/')
})

app.get('/forgotPassword', function (req, res) {
  res.render('session/forgotPassword')
})

app.post('/forgotPassword', function (req, res, next) {
  Usuario.findOne({ email: req.body.email }, function (err, usuario) {
    if (!usuario) return res.render('session/forgotPassword', { info: { message: 'El email no esta registrado' } })

    usuario.resetPassword(function (err) {
      if (err) return next(err)
      console.log('session.forgotPasswordMessage')
    })

    res.render('session/forgotPasswordMessage')
  })
})

app.get('/resetPassword/:token', function (req, res, next) {
  Token.findOne({ token: req.params.token }, function (err, token) {
    if (!token) return res.status(400).send({ type: "no-verified", info: { message: 'No existe un usuario asociado al token. Verifique que su token no haya expirado.' } })

    Usuario.findById(token._userId, function (err, usuario) {
      if (!usuario) return res.status(400).send({ info: { message: 'No existe un usuario asociado al token.' } })
      res.render('session/resetPassword', { erros: {}, usuario: usuario })
    })
  })
})

app.post('/resetPassword', function (req, res) {

  if (req.body.password != req.body.confirm_password) {
    res.render(
      'session/resetPassword',
      {
        info: { message: 'No coincide con la password ingresada' },
        usuario: new Usuario({ email: req.body.email })
      }
    )
    return
  }

  Usuario.findOne({ email: req.body.email }, function (err, usuario) {
    usuario.password = req.body.password
    usuario.save(function (err) {
      if (err) {
        res.render('session/resetPassword', { usuario: new Usuario({ email: req.body.email }) })
      } else {
        res.redirect('/login')
      }
    })
  })
})
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn (req, res, next) {
  if (req.user) {
    next()
  } else {
    console.log('Usuario sin logearse')
    res.redirect('/login')
  }
  // console.log(req.user)
}

function validarUsuario (req, res, next) {
  const token = req.headers['x-access-token']
  if (!token) {
    return res.status(401).json({
      auth: false,
      message: 'No token provided, Denied access'
    })
  }

  try {
    const decoded = jwt.verify(token, req.app.get('secretKey'))
    req.userId = decoded.id
    next()
  } catch (err) {
    res.status(400).json({ status: 'error', message: err.message, data: null })
  }
}
//base de datos 

mon.connect('mongodb+srv://tvbotbdproyecto:5UV2qVL3XWrlL76k@cluster0.7h1um.mongodb.net/tvvideo?retryWrites=true&w=majority', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
})
  .then(db => { console.log('Se conecto a mongo Atlas') })
  .catch(err => { console.error(err) });


// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
