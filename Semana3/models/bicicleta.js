
const mon = require('mongoose');

const { Schema, model } = mon;
const { ObjectId } = Schema;




const biciSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: { type: '2dsphere', sparse: true }
    }
});

biciSchema.statics.createInstace = function (code, color, modelo, ubicacion) {
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

biciSchema.methods.toString = function () {
    return ("code " + this.code + "color " + this.color);
};
biciSchema.statics.allBicis = async function (cb) {
    //console.log(this.find({}, cb))
    return await this.find({}, cb);
}

biciSchema.statics.add = function (abici, cb) {
    this.create(abici, cb);
}
biciSchema.statics.findBycode = function (aCode, cb) {
    return this.findOne({ code: aCode }, cb);
}

biciSchema.statics.removeByCode = function (aCode, cb) {
    return this.deleteOne({ code: aCode }, cb);
}

module.exports = model('Bicicleta', biciSchema);

/*
var Bicicleta = function (id, color, modelo, ubicacion) {
    this.id = id,
        this.color = color,
        this.modelo = modelo,
        this.ubicacion = ubicacion
}
Bicicleta.prototype.toString = function () {
    return 'id: ' + this.id + " | color: " + this.color;
}

Bicicleta.allBicis = [];

Bicicleta.add = function (aBicis) {
    Bicicleta.allBicis.push(aBicis);
}

Bicicleta.findByid = function (id) {
    //console.log(Bicicleta.allBicis);
    var bici = Bicicleta.allBicis.find(x => {
        if (x.id == id) return x;
    });

    if (bici) { return bici; }
    else { throw new Error(`No se encontro la bicicleta ${id}`) }
}

Bicicleta.removeByid = function (id) {
    var bici = Bicicleta.findByid(id);
    for (var i = 0; i < Bicicleta.allBicis.length; i++) {
        if (Bicicleta.allBicis[i].id == id) {

            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
}

module.exports = Bicicleta; */