const mon = require('mongoose');
const Reserva = require('./reserva');
const uniqueValidator = require('mongoose-unique-validator');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const { Schema, model } = mon;
const Token = require('../models/token');
const mailer = require('../mailer/mailer');

const saltRounds = 10;
const validatioEmail = function (email) {
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email)
};

const usuarioScehma = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es requerido']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        unique: true,
        lowercase: true,
        validate: [validatioEmail, 'Ingrese un email valido'],
    },
    password: {
        type: String,
        trim: true,
        required: [true, 'La password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpiress: Date,
    verificado: {
        type: Boolean,
        default: false
    }
});

usuarioScehma.plugin(uniqueValidator, { message: `EL {PATH} ya existe con otro usuario` })

usuarioScehma.pre('save', function (next) {
    if (this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
})

usuarioScehma.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);// comprueba si el encriptado esta bien echo entre el resultante y el planeado 
}
usuarioScehma.methods.reservar = function (biciId, desde, hasta, cb) {
    var reserva = new Reserva({ usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta })
    console.log(reserva);
    reserva.save(cb);
}

usuarioScehma.methods.enviar_email_bienvenida = function (cb) {
    const token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString('hex') })
    const email_destination = this.email
    token.save(function (err) {
        if (err) { return console.log(err.message) }

        // configuracion email
        const mailOptions = {
            from: 'no-reply@eliasmoura.com',
            to: email_destination,
            subject: 'Verificacion de cuenta',
            text: 'Hola,\n\n' + 'Por favor, para verificar su cuenta haga click en este link: \n' + 'http://localhost:5000' + '\/token/confirmation\/' + token.token + '.\n'
        }

        mailer.sendMail(mailOptions, function (err) {
            if (err) { return console.log(err.message) }

            console.log('A verification email has been to sent to ' + email_destination + '.')
            // Se ha enviado un email de bienvenida a
        })
    })
}


usuarioScehma.method('resetPassword', function (cb) {
    const token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString('hex') })
    const email_destination = this.email
    token.save(function (err) {
        if (err) { return cb(err) }

        // configuracion email
        const mailOptions = {
            from: 'no-reply@eliasmoura.com',
            to: email_destination,
            subject: 'Reseteo de password',
            text: 'Hola,\n\n' + 'Por favor, para resetear la password de su cuenta haga click en este link: \n' + 'http://localhost:5000' + '\/resetPassword\/' + token.token + '.\n'
        }

        mailer.sendMail(mailOptions, function (err) {
            if (err) { return cb(err) }

            console.log('Se envio un email para resetear la password a: ' + email_destination + '.')
            // Se ha enviado un email de bienvenida a
        })

        cb(null)
    })
})

module.exports = model("usuariosbici", usuarioScehma); 