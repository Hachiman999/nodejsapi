var Bicicleta = function(id,color,modelo,ubicacion){
    this.id = id,
    this.color = color,
    this.modelo = modelo,
    this.ubicacion = ubicacion
}
Bicicleta.prototype.toString = function(){
    return 'id: '+ this.id+ " | color: " + this.color; 
}

Bicicleta.allBicis = [];

Bicicleta.add = function(aBicis){
    Bicicleta.allBicis.push(aBicis); 
}

Bicicleta.findByid= function(id){
    //console.log(Bicicleta.allBicis); 
    var bici = Bicicleta.allBicis.find(x=> {
        if(x.id == id) return x ; 
    });

    if(bici){return bici;}
    else{ throw new Error( `No se encontro la bicicleta ${id}`)}
}

Bicicleta.removeByid= function(id){
    var bici = Bicicleta.findByid(id);
    for(var i=0; i< Bicicleta.allBicis.length; i++ ){
        if(Bicicleta.allBicis[i].id== id){
            
            Bicicleta.allBicis.splice(i,1); 
            break; 
        }
    }
}

var a = new Bicicleta(1,'rojo','urbana',[3.5285844,-76.2809306,21] )

Bicicleta.add(a); 
module.exports = Bicicleta; 