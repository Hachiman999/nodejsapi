const Bicicleta = require("../../models/bicicleta"); 

exports.bicicleta_list = function(req,res){
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
}

exports.bicicleta_create = function(req,res){
    const {id, color, modelo, lat,log} = req.body; 
    var bici = new Bicicleta (id,color,modelo); 
    bici.ubicacion = [lat,log]; 

    Bicicleta.add(bici); 
    res.Status(200).json({
        bicicleta: bici
    });
}